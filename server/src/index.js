const { GraphQLServer } = require('graphql-yoga')
const { Prisma } = require('prisma-binding')
const sendSMS = require('../src/useCases/sendSms')

const resolvers = {
    Query: {
        user: (_, { id }, context, info) => {
            return context.prisma.query.user(
                {
                    where: {
                        id
                    },
                },
                info,
            )
        },
        transactions: (_, { cpfOrCnpj }, context, info) => {
            return context.prisma.query.transactions(
                {
                    where: {
                        cnpjBuyer: cpfOrCnpj
                    },
                },
                info,
            )
        }
    },
    Mutation: {
        createTransaction: (_, {
            cnpjBuyer,
            cnpjVendor,
            transactionValue,
            transactionDateTime,
            transactionExpectedPaymentDate,
            transactionPaymentDate,
            transactionPaymentStatus
        }, context, info) => {
            return context.prisma.mutation.createTransaction({
                data: {
                    cnpjBuyer,
                    cnpjVendor,
                    transactionValue,
                    transactionDateTime,
                    transactionExpectedPaymentDate,
                    transactionPaymentDate,
                    transactionPaymentStatus
                },
            }, info)
        },
        signup: (_, {
            name,
            cnpj,
            phone,
            address,
            limit,
            alreadyUsedLimit,
            anual
        }, context, info) => {
            return context.prisma.mutation.createUser(
                {
                    data: {
                        name,
                        cnpj,
                        phone,
                        address,
                        limit,
                        alreadyUsedLimit,
                        anual
                    },
                }, info)
        },

        sendSms: (_, { phone, name, transactionDate, transactionValue }, __, ___) => sendSMS({ phone, name, transactionDate, transactionValue })
    }
}

const server = new GraphQLServer({
    typeDefs: 'src/schema.graphql',
    resolvers,
    context: req => ({
        ...req,
        prisma: new Prisma({
            typeDefs: 'src/generated/prisma.graphql',
            endpoint: 'http://localhost:4466',
        }),
    }),
})
server.start(() => console.log(`GraphQL server is running on http://localhost:4000`))