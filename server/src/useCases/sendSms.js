const TeleSignSDK = require('telesignsdk');
const { customerId, apiKey, rest_endpoint, messageType } = require('../../infra/config/telesign')
const client = new TeleSignSDK(customerId, apiKey, rest_endpoint);

async function sendSMS({ phone, name, transactionDate, transactionValue }) {
    const message = `${name}, consta em aberto uma fatura de ${transactionDate} no valor de R$ ${transactionValue}. Cód. barras 0000... Desconsiderar se pago`;
    const result = await client.sms.message((error, responseBody) => console.log(`Messaging response for messaging phone number: ${phone}` +
        ` => code: ${responseBody['status']['code']}` + `, description: ${responseBody['status']['description']}`), phone, message, messageType)
}

module.exports = sendSMS